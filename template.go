package main

import (
	"bytes"
	"errors"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/Strubbl/wallabago/v9"
)

// replace all unicode white spaces with one blank and trim leading and trailing spaces
func condenseWhitespaces(s string) string {
	return strings.Join(strings.Fields(strings.TrimSpace(s)), " ")
}

// each field of the string slice is added in one string, where each slice is separated by a comma and a blank
func getArrayAsCommaSeparatedString(s []string) string {
	returnString := ""
	for i := 0; i < len(s); i++ {
		if strings.TrimSpace(s[i]) == "" {
			continue
		}
		returnString = returnString + "[[" + condenseWhitespaces(s[i]) + "]]"
		if i != len(s)-1 {
			returnString = returnString + ", "
		}
	}
	return returnString
}

// convert slice of wallabag tags to a comma-separted tag string
func getTagsAsCommaSeparatedString(t []wallabago.Tag) string {
	var tags []string
	for i := 0; i < len(t); i++ {
		tags = append(tags, t[i].Label)
	}
	return getArrayAsCommaSeparatedString(tags)
}

func getWallabagURL(id int) string {
	return wallabago.LibConfig.WallabagURL + "/view/" + strconv.Itoa(id)
}

func linkDayOfDate(t wallabago.WallabagTime) string {
	originalFormat := t.Time.Format(time.DateTime)
	rfcFormat := t.Time.Format(time.DateOnly)
	return strings.ReplaceAll(originalFormat, rfcFormat, "[["+rfcFormat+"]]")
}

// converts each newline to a newline with numberOfTabs tabs and two blanks
// this is needed to indent paragraphs in the markdown file in line with the bullet point
func newlineToTabs(text string, numberOfTabs int) string {
	var tabs string
	twoSpaces := "  "
	for i := 0; i < numberOfTabs; i++ {
		tabs += "\t"
	}
	return strings.ReplaceAll(text, "\n", "\n"+tabs+twoSpaces)
}

func getArticleMarkdown(article wallabago.Item, articleTemplate string, tags []string) (string, error) {
	var buf bytes.Buffer
	funcMap := template.FuncMap{
		"arrayToCommaSeparatedString": getArrayAsCommaSeparatedString,
		"condenseWhitespaces":         condenseWhitespaces,
		"linkDate":                    linkDayOfDate,
		"newlineToTabs":               newlineToTabs,
		"tagsToCommaSeparatedString":  getTagsAsCommaSeparatedString,
		"wallabagURL":                 getWallabagURL,
	}
	tmpl, err := template.New("").Funcs(funcMap).Parse(articleTemplate)
	if err != nil {
		return "", err
	}
	err = tmpl.Execute(&buf, article)
	if err != nil {
		return "", err
	}
	s := buf.String()
	s, err = cleanArticleMarkdown(s)
	if err != nil {
		return "", err
	}
	s = autoTagText(s, tags)
	if *debugDebug {
		log.Printf("getArticleMarkdown: autoTagText:\n%v\n", s)
	}
	return s, err
}

// clean up the markdown of the article, e.g.
// shrink two newlines to one
// remove unicode white spaces used for hyphenation
// replace non-breaking space with a regular space
func cleanArticleMarkdown(s string) (string, error) {
	if s == "" {
		return s, nil
	}
	// replace two or more consecutive newlines by one newline
	twoNewlines := "\n\n"
	regex, err := regexp.Compile(twoNewlines)
	if err != nil {
		return "", err
	}
	for strings.Contains(s, twoNewlines) {
		s = regex.ReplaceAllString(s, "\n")
	}
	// replace NBSP unicode character [U+00A0] with a space
	s = strings.ReplaceAll(s, " ", " ")
	// replace soft hyphen unicode character [U+00AD] with nothing
	s = strings.ReplaceAll(s, "\u00ad", "")
	return s, nil
}

func appendArticleToMdFile(article wallabago.Item, fileName string, pageTemplate string, articleTemplate string, autoTags []string) error {
	pathExists := true
	if _, err := os.Stat(fileName); errors.Is(err, os.ErrNotExist) {
		pathExists = false
	}
	f, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		if *debugDebug {
			log.Println("while os.OpenFile for file", fileName, "there was an error:", err)
		}
		return err
	}
	defer f.Close()

	if !pathExists {
		n, err := f.WriteString(pageTemplate)
		if *debug {
			log.Println("appendArticleToMdFile has written", n, "bytes initially for the page template")
		}
		if err != nil {
			return err
		}
	}
	articleMd, err := getArticleMarkdown(article, articleTemplate, autoTags)
	if err != nil {
		return err
	}
	n, err := f.WriteString(articleMd)
	if *debugDebug {
		log.Println("appendArticleToMdFile has appended", n, "bytes")
	}
	if err != nil {
		return err
	}
	return err
}

func readFile(fp string) (string, error) {
	if *debugDebug {
		log.Println("readFile: file path is", fp)
	}
	var templateContent string
	if _, err := os.Stat(fp); os.IsNotExist(err) {
		if *verbose { // not fatal, just start with a new one
			log.Printf("file does not exist %s", fp)
		}
		return templateContent, &FileDoesNotExistError{}
	}
	buf, err := os.ReadFile(fp)
	if err != nil {
		return templateContent, err
	}
	templateContent = string(buf)
	return templateContent, nil
}

func writeFile(t string, fp string) error {
	if *debugDebug {
		log.Println("writeFile: fp:", fp)
	}
	err := os.WriteFile(fp, []byte(t), 0644)
	if err != nil {
		return err
	}
	return nil
}
