package main

const (
	defaultAutoTag1                = "wallabag"
	defaultAutoTag2                = "Logseq"
	defaultAutoTagsFilePath        = "auto-tags.txt"
	defaultConfigJSON              = "config.json"
	defaultCachingJSON             = "caching.json"
	defaultOutputPath              = "wallabag-articles.md"
	defaultTemplateFilePathArticle = "t-article.template"
	defaultTemplateFilePathPage    = "t-page.template"
	defaultWbgoConfigClientID      = "your-client-id"
	defaultWbgoConfigClientSecret  = "your-client-secret"
	defaultWbgoConfigWallabagURL   = "https://your-wallabag-url"
	defaultWbgoConfigUserName      = "your-user-name"
	defaultWbgoConfigUserPassword  = "your-user-password"
	defaultPageTemplate            = `tags:: wallabag, export

`
	defaultArticleTemplate = `- ### {{condenseWhitespaces .Title}}
  collapsed:: false
  url:: {{if .OriginURL}}{{.OriginURL}}{{else if .GivenURL}}{{.GivenURL}}{{else}}{{.URL}}{{end}}
  wallabag-url:: {{wallabagURL .ID}}
{{if .Tags}}  tags:: {{tagsToCommaSeparatedString .Tags}}{{end}}
{{if .DomainName}}  site:: [[{{.DomainName}}]]{{end}}
{{if .PublishedBy}}  author:: {{arrayToCommaSeparatedString .PublishedBy}}{{end}}
{{if .PublishedAt}}  date-published:: {{linkDate .PublishedAt}}{{end}}
{{if .CreatedAt}}  date-saved:: {{linkDate .CreatedAt}}{{end}}
{{if .ArchivedAt}}  date-archived:: {{linkDate .ArchivedAt}}{{end}}
{{range .Annotations}}
{{if .Text}}	- {{newlineToTabs .Text 1}}
		- {{newlineToTabs .Quote 2}}{{else}}
	- {{newlineToTabs .Quote 1}}{{end}}{{end}}
`
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
	builtBy = "unknown"
)
