package main

import (
	"testing"
)

func TestNewlineToTabs(t *testing.T) {
	var tests = []struct {
		test   string
		no     int
		result string
	}{
		{"test \noejdnfoewnf wefowenfo", 3, "test \n\t\t\t  oejdnfoewnf wefowenfo"},
		{"test \noejdnfoewnf wefowenfo", 2, "test \n\t\t  oejdnfoewnf wefowenfo"},
		{"test \noejdnfoewnf wefowenfo", 1, "test \n\t  oejdnfoewnf wefowenfo"},
		{"test \noejdnfoewnf wefowenfo", 0, "test \n  oejdnfoewnf wefowenfo"},
	}
	for _, test := range tests {
		r := newlineToTabs(test.test, test.no)
		if r != test.result {
			t.Errorf("TestNewlineToTabs: expectedResult %v, got %v\n", test.result, r)
		}
	}
}

func TestCleanArticleMarkdown(t *testing.T) {
	var tests = []struct {
		test   string
		result string
	}{
		// test newline replacement
		{"test \n\noejdnfoewnf wefowenfo", "test \noejdnfoewnf wefowenfo"},
		{"test \n\n\noejdnfoewnf wefowenfo", "test \noejdnfoewnf wefowenfo"},
		{"test \n\n\n\noejdnfoewnf wefowenfo", "test \noejdnfoewnf wefowenfo"},
		// test unicode NBSP replacement
		{"test oejd nf o ewnfwe fow enfo", "test oejd nf o ewnfwe fow enfo"},
	}
	for _, test := range tests {
		r, err := cleanArticleMarkdown(test.test)
		if err != nil {
			t.Errorf("TestCleanArticleMarkdown: got error: %v\n", err)
		}
		if r != test.result {
			t.Errorf("TestCleanArticleMarkdown: expectedResult %v, got %v\n", test.result, r)
		}
	}
}
