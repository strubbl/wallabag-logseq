package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

var debug = flag.Bool("d", false, "get debug output (includes verbose mode)")
var debugDebug = flag.Bool("dd", false, "get even more debug output (includes debug mode)")
var learnTags = flag.Bool("learn-auto-tags", false, "prints the tags from the Logseq's wallabag markdown file combined with already existing tags from your config, result is printed ordered by length and then alphabetically")
var v = flag.Bool("v", false, "print version")
var verbose = flag.Bool("verbose", false, "verbose mode")
var configJSON = flag.String("config", defaultConfigJSON, "path to config JSON file")

func handleFlags() {
	flag.Parse()
	if *debug && len(flag.Args()) > 0 {
		log.Printf("handleFlags: non-flag args=%v", strings.Join(flag.Args(), " "))
	}
	// version first, because it directly exits here
	if *v {
		fmt.Printf("wallabag-logseq version %s, commit %s, built at %s by %s\n", version, commit, date, builtBy)
		os.Exit(0)
	}
	// test verbose before debug because debug includes verbose
	if *verbose && !*debug && !*debugDebug {
		log.Printf("verbose mode")
	}
	if *debug && !*debugDebug {
		log.Printf("handleFlags: debug mode")
		// debug includes verbose
		*verbose = true
	}
	if *debugDebug {
		log.Printf("handleFlags: debug² mode")
		// debugDebug includes debug
		*debug = true
		// and debug includes verbose
		*verbose = true
	}
}
