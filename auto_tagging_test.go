package main

import (
	"testing"
)

func TestGetTagsFromString(t *testing.T) {
	var tests = []struct {
		test   string
		result []string
	}{
		{"test oejdnfoewnf wefowenfo", []string{}},
		{"asdf [[test]] alsdmk", []string{"test"}},
		{"asdf [[test]] [[tesT]] [[TEST]] [[test1]] [[test2]] alsdmk", []string{"test", "tesT", "TEST", "test1", "test2"}},
		{"", []string{}},
		{`asdf [[test]] [[tesT]] [[TEST]]
		[[test1]]
		[[test2]] alsdmk`, []string{"test", "tesT", "TEST", "test1", "test2"}},
		{`- asdf [[test]] [[tesT]]
		tags:: [[TEST]]
		[[test1]]
		[[test2]] alsdmk`, []string{"test", "tesT", "test1", "test2"}},
		{"[[asdf]] [test]] [[tesT] [TEST] [test1 test2] alsdmk", []string{"asdf"}},
	}
	for _, test := range tests {
		r, err := getTagsFromText(test.test)
		if err != nil {
			t.Error("TestGetTagsFromString: got error while getTagsFromString", err)
		}
		if len(r) != len(test.result) {
			t.Errorf("TestGetTagsFromString: expected len(expectedResult)=%v, got len(result)=%v\n", len(test.result), len(r))
		}
		for i := 0; i < len(r); i++ {
			if r[i] != test.result[i] {
				t.Errorf("TestGetTagsFromString: expectedResult %v, got %v\n", test.result[i], r[i])
			}
		}
	}

}

func TestMergeTagsSortUniqExpectedLengths(t *testing.T) {
	var tests = []struct {
		t1     []string
		t2     []string
		result []string
	}{
		{[]string{}, []string{}, []string{}},
		{[]string{"wallabag", "Logseq"}, []string{"codeberg", "tree", "Test", "TEST", "teXt", "test", "text"}, []string{"codeberg", "wallabag", "Logseq", "Test", "TEST", "teXt", "tree"}},
		{[]string{"wallabag", "Logseq"}, []string{}, []string{"wallabag", "Logseq"}},
		{[]string{}, []string{"codeberg", "tree", "Test", "TEST", "teXt", "test", "text"}, []string{"codeberg", "Test", "TEST", "teXt", "tree"}},
	}

	for k, test := range tests {
		r := mergeTagsSortUniq(test.t1, test.t2)
		if len(r) != len(test.result) {
			t.Errorf("TestMergeTagsSortUniqExpectedLengths: test %d failed, expected len(expectedResult)=%v, got len(result)=%v\nr=%v\nexpectedResult=%v", k, len(test.result), len(r), r, test.result)
			continue
		}
		for i := 0; i < len(r); i++ {
			if r[i] != test.result[i] {
				t.Errorf("TestMergeTagsSortUniqExpectedLengths: test %d failed, expectedResult %v, got %v\n", k, test.result[i], r[i])
				t.Errorf("TestMergeTagsSortUniqExpectedLengths: expectedResult=%v, got result=%v", test.result, r)
			}
		}
	}
}

var text1 = `- ### Android: Was ist NFC-Tags alles so machbaro
  url:: https://testURL/NFC/SMS/asdf.html
  collapsed:: false
  wallabag-url:: /view/12345
  tags:: [[android]], [[nfc]]
  site:: [[testURL]]
  author:: 
  date-saved:: [[1970-01-01]] 12:12:12
  date-archived:: [[1970-01-01]] 12:12:12
  - Die App NFC Tagwriter by NXP beschreibt sie universell. Unter anderem folgende Befehle sind möglich:Kontaktdaten hinterlegen
Mit geschütztem WLAN verbinden
Eine URL aufrufen
Mit bestimmtem Bluetooth-Gerät verbinden
E-Mail verschicken
Telefonnummer anrufen
Eine App starten
geschriebenen Text vorlesen
SMS versenden`

var tags1 = []string{"Android", "URL", "WLAN", "SMS", "e-mail"}
var result1 = `- ### [[Android]]: Was ist NFC-Tags alles so machbaro
  url:: https://testURL/NFC/SMS/asdf.html
  collapsed:: false
  wallabag-url:: /view/12345
  tags:: [[android]], [[nfc]]
  site:: [[testURL]]
  author:: 
  date-saved:: [[1970-01-01]] 12:12:12
  date-archived:: [[1970-01-01]] 12:12:12
  - Die App NFC Tagwriter by NXP beschreibt sie universell. Unter anderem folgende Befehle sind möglich:Kontaktdaten hinterlegen
Mit geschütztem [[WLAN]] verbinden
Eine [[URL]] aufrufen
Mit bestimmtem Bluetooth-Gerät verbinden
[[e-mail]] verschicken
Telefonnummer anrufen
Eine App starten
geschriebenen Text vorlesen
[[SMS]] versenden
`

func TestAutoTagText(t *testing.T) {
	var tests = []struct {
		text   string
		tags   []string
		result string
	}{
		{"", []string{}, ""},
		{"there is a new tool in town connecting wallabag and Logseq", []string{"wallabag", "Logseq"}, "there is a new tool in town connecting [[wallabag]] and [[Logseq]]\n"},
		{"there is a new tool in town connecting wallabag and Logseq or wallabag and Logseq?", []string{"wallabag", "Logseq"}, "there is a new tool in town connecting [[wallabag]] and [[Logseq]] or wallabag and Logseq?\n"},
		{"there is a new tool in town connecting Wallabag and logseq", []string{"wallabag", "Logseq"}, "there is a new tool in town connecting [[wallabag]] and [[Logseq]]\n"},
		{"there is a new tool in town:: connecting Wallabag and logseq", []string{"wallabag", "Logseq"}, "there is a new tool in town:: connecting Wallabag and logseq\n"},
		{"there is a new tool in town connecting wallabag. and Logseq", []string{"wallabag", "Logseq"}, "there is a new tool in town connecting [[wallabag]]. and [[Logseq]]\n"},
		{"there is a new tool in town connecting .wallabag and Logseq", []string{"wallabag", "Logseq"}, "there is a new tool in town connecting .[[wallabag]] and [[Logseq]]\n"},
		{"https://logseq.com", []string{"wallabag", "Logseq"}, "https://logseq.com\n"},
		{"https://www.logseq.com", []string{"wallabag", "Logseq"}, "https://www.logseq.com\n"},
		{"https://www.OpenStreetMap.com/logseq/asdf", []string{"wallabag", "Logseq"}, "https://www.OpenStreetMap.com/logseq/asdf\n"},
		{"see my new URL https://www.OpenStreetMap.com/my-logseq.asdf in your favorite browser", []string{"wallabag", "Logseq", "https"}, "see my new URL https://www.OpenStreetMap.com/my-logseq.asdf in your favorite browser\n"},
		{"logseq://OpenStreetMap.com", []string{"wallabag", "Logseq"}, "logseq://OpenStreetMap.com\n"},
		{"there is a new tool in town connecting my-wallabag, wallabag and Logseq", []string{"my-wallabag", "wallabag", "Logseq"}, "there is a new tool in town connecting [[my-wallabag]], [[wallabag]] and [[Logseq]]\n"},
		// TODO following test fails, see also https://codeberg.org/strubbl/wallabag-logseq/issues/4
		// {"zugänglich", []string{"Zug", "wallabag", "Logseq"}, "zugänglich\n"},
		{text1, tags1, result1},
		//{text2, tags1, result2}, // not implemented yet
	}

	for k, test := range tests {
		r := autoTagText(test.text, test.tags)
		if r != test.result {
			t.Errorf("TestAutoTagText: test %d failed, expectedResult: \n%v\n but got: \n%v\n", k, test.result, r)
		}
	}
}

func TestReplaceFirstTag(t *testing.T) {
	var tests = []struct {
		text       string
		tag        string
		tagReplace string
		result     string
	}{
		{"", "", "", ""},
		{"there is a new tool in town connecting wallabag and Logseq", "wallabag", "[[wallabag]]", "there is a new tool in town connecting [[wallabag]] and Logseq"},
		{"there is a new tool in town connecting WALLABAG and Logseq", "wallabag", "[[wallabag]]", "there is a new tool in town connecting WALLABAG and Logseq"},
		{"there is a new tool in town connecting WALLABAG and Logseq", "WALLABAG", "[[WALLABAG]]", "there is a new tool in town connecting [[WALLABAG]] and Logseq"},
		{"there is a new tool in town connecting wallabag, WALLABAG and Logseq", "WALLABAG", "[[WALLABAG]]", "there is a new tool in town connecting wallabag, [[WALLABAG]] and Logseq"},
		{"there is a new tool in town connecting WALLABAG, wallabag and Logseq", "wallabag", "[[wallabag]]", "there is a new tool in town connecting WALLABAG, [[wallabag]] and Logseq"},
		{"there is a new tool in town connecting wallabag and Logseq", "Logseq", "[[Logseq]]", "there is a new tool in town connecting wallabag and [[Logseq]]"},
		{"there is a new tool in town connecting wallabag and logseq", "Logseq", "[[Logseq]]", "there is a new tool in town connecting wallabag and [[Logseq]]"},
		{"there is a new tool in town connecting wallabag and Logseq and wallabag", "wallabag", "[[wallabag]]", "there is a new tool in town connecting [[wallabag]] and Logseq and wallabag"},
		{"there is a new tool in town connecting w, wa, wal, wall, walla, wallab, wallaba, wallabag and Logseq and wallabag", "wallabag", "[[wallabag]]", "there is a new tool in town connecting w, wa, wal, wall, walla, wallab, wallaba, [[wallabag]] and Logseq and wallabag"},
		{"tags in tags shall not be allowed [[wallabag-theme]] and Logseq and wallabag", "wallabag", "[[wallabag]]", "tags in tags shall not be allowed [[wallabag-theme]] and Logseq and [[wallabag]]"},
		{"not allowed [[wallabag-theme]] and Logseq and [[wallabag]]", "wallabag", "[[wallabag]]", "not allowed [[wallabag-theme]] and Logseq and [[wallabag]]"},
		{"not allowed [[wallabag-theme]] and [[wallabag]] and waLLabag", "wallabag", "[[wallabag]]", "not allowed [[wallabag-theme]] and [[wallabag]] and waLLabag"},
		{"TLP:AMBER+STRICT schließt Kunden aus", "TLP:AMBER+STRICT", "[[TLP:AMBER+STRICT]]", "[[TLP:AMBER+STRICT]] schließt Kunden aus"},
	}

	for k, test := range tests {
		r := replaceFirstTag(test.text, test.tag, test.tagReplace)
		if r != test.result {
			t.Errorf("TestReplaceFirstTagCaseInsensitive: test %d failed, expectedResult: %v\n but got: %v\n", k, test.result, r)
		}
	}
}

func TestGetAllTagOccurances(t *testing.T) {
	var tests = []struct {
		text   string
		tag    string
		result []int
	}{
		{"", "", []int{0}},
		{"", "wallabag", []int{}},
		{"there is a new tool in town connecting wallabag and Logseq", "wallabag", []int{39}},
		{"there is a new tool in town connecting w, wa, wal, wall, walla, wallab, wallaba, wallabag and Logseq and wallabag", "wallabag", []int{81, 105}},
		{"there is a new tool in town connecting w, wa, wal, wall, walla, wallab, wallaba, wallabag, WALLABAG and Logseq and wallabag", "wallabag", []int{81, 115}},
		{"tags in tags shall not be allowed [[wallabag-theme]] and Logseq and wallabag", "wallabag", []int{36, 68}},
		{"TLP:AMBER+STRICT schließt Kunden aus", "TLP:AMBER+STRICT", []int{0}},
	}

	for k, test := range tests {
		r := getAllTagOccurances(test.text, test.tag)
		if len(r) != len(test.result) {
			t.Errorf("TestGetAllTagOccurances: test %d failed, expected len(expectedResult)=%v, got len(result)=%v\nr=%v\nexpectedResult=%v", k, len(test.result), len(r), r, test.result)
			continue
		}
		for i := 0; i < len(r); i++ {
			if r[i] != test.result[i] {
				t.Errorf("TestGetAllTagOccurances: test %d failed, expectedResult %v, got %v\n", k, test.result[i], r[i])
			}
		}

	}
}

func TestIsTagTheWholeWord(t *testing.T) {
	var tests = []struct {
		text      string
		tag       string
		occurance int
		result    bool
	}{
		{"", "", 0, false},
		{"", "wallabag", 0, false},
		{"there is a new tool in town connectinx wallabag or Logseq", "wallabag", -5, false},
		{"there is a new tool in town connectinx wallabag or Logseq", "wallabag", 39, true},
		{"there is a new tool in town connectinxwallabag or Logseq", "wallabag", 38, false},
		{"there is a new tool in town connectinx wallabagor Logseq", "wallabag", 39, false},
		{"there is a new tool in town connectinxwallabagor Logseq", "wallabag", 38, false},
		{"dies ist öin wallabag täst", "wallabag", 14, true},
		{"diesköinäwallabag ötäst", "wallabag", 11, false},
		{"dieß ist öin walläbag täst", "walläbag", 15, true},
	}
	for k, test := range tests {
		r := isTagTheWholeWord(test.text, test.tag, test.occurance)
		if r != test.result {
			t.Errorf("TestGetAllTagOccurances: test %d failed, expectedResult %v, got %v\n", k, test.result, r)
		}
	}
}

func TestGetLastRune(t *testing.T) {
	var tests = []struct {
		text   string
		result rune
	}{
		{"ströng", []rune("g")[0]},
		{"waaä", []rune("ä")[0]},
	}
	for k, test := range tests {
		r := getLastRune(test.text)
		if r != test.result {
			t.Errorf("TestGetAllTagOccurances: test %d failed, expectedResult %v, got %v\n", k, test.result, r)
		}
	}
}
