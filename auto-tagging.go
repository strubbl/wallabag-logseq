package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"
)

func learnTagsForAutoTagging(c WLConfig, autoTags *[]string) error {
	if c.LogseqMarkdownFilePath == "" {
		return nil
	}
	content, err := readFile(c.LogseqMarkdownFilePath)
	if err != nil {
		log.Println("error while reading the markdown file:", err)
		return err
	}
	tags, err := getTagsFromText(content)
	if err != nil {
		return err
	}
	if *debugDebug {
		log.Println("learnTagsForAutoTagging: found tags in markdown file:", tags)
		log.Println("learnTagsForAutoTagging: found tags in auto tags file:", autoTags)
	}
	// merge config tags and found tags
	final_tags := mergeTagsSortUniq(*autoTags, tags)
	*autoTags = final_tags
	return nil
}

func getTagsFromText(s string) ([]string, error) {
	var tags []string
	for _, line := range strings.Split(strings.TrimSuffix(s, "\n"), "\n") {
		if strings.Contains(line, "::") {
			if *debugDebug {
				log.Println("getTagsFromString: ignoring line with double colons", line)
			}
			continue
		}
		var re = regexp.MustCompile(`(?mU)\[\[(.*)\]\]`)
		matches := re.FindAllStringSubmatch(line, -1)
		for _, match := range matches {
			if *debugDebug {
				log.Println("match found:", match)
			}
			tags = append(tags, match[1])
		}
	}
	return tags, nil
}

func mergeTagsSortUniq(s1 []string, s2 []string) []string {
	merged_tags := make([]string, len(s1))
	copy(merged_tags, s1)
	merged_tags = append(merged_tags, s2...)
	if *debugDebug {
		log.Println("mergeTagsSortUniq: found tags (merged with config):", merged_tags)
	}
	var rememberProcessedTags = make(map[string]bool)
	var unique_tags = []string{}

	for i := 0; i < len(merged_tags); i++ {
		// test if the given tag is all upper case letters
		if merged_tags[i] == strings.ToUpper(merged_tags[i]) {
			if rememberProcessedTags[merged_tags[i]] {
				// all upper case tag exists already
				continue
			}
			rememberProcessedTags[merged_tags[i]] = true
		} else {
			// tag is all lower case or mixed case, but not all upper case
			loweredTag := strings.ToLower(merged_tags[i])
			if rememberProcessedTags[loweredTag] {
				continue
			}
			rememberProcessedTags[loweredTag] = true
		}
		unique_tags = append(unique_tags, merged_tags[i])
	}
	if *debugDebug {
		log.Println("mergeTagsSortUniq: found unique tags:", unique_tags)
	}
	sort.Slice(unique_tags, func(i, j int) bool {
		if len(unique_tags[i]) == len(unique_tags[j]) {
			// ignore case when sorting, otherwise the lower case results appear only
			// after all upper case results and not in between in alphabetical order
			return strings.ToLower(unique_tags[i]) < strings.ToLower(unique_tags[j])
		}
		return len(unique_tags[i]) > len(unique_tags[j])
	})
	return unique_tags
}

func autoTagText(s string, tags []string) string {
	if s == "" {
		return s
	}
	replacedText := ""
	for _, line := range strings.Split(strings.TrimSuffix(s, "\n"), "\n") {
		if strings.Contains(line, "::") {
			if *debugDebug {
				log.Println("autoTagText: ignoring line with double colons:", line)
			}
			replacedText += line + "\n"
			continue
		}
		for i := 0; i < len(tags); i++ {
			line = replaceFirstTag(line, tags[i], "[["+tags[i]+"]]")
		}
		replacedText += line + "\n"
		if *debugDebug {
			log.Println("autoTagText: applied all tags to line:", line)
		}
	}
	return replacedText
}

func replaceFirstTag(s string, tag string, tagReplace string) string {
	if *debugDebug {
		log.Println("s:", s, "tag:", tag, "tagReplace:", tagReplace)
	}
	if strings.Contains(s, tagReplace) {
		return s
	}
	tagOccurances := getAllTagOccurances(s, tag)
	if *debugDebug {
		log.Println("replaceFirstTag: searching for tag", tag, "in string", s)
		log.Println("replaceFirstTag: tagOccurances:", tagOccurances)
	}
	for i := 0; i < len(tagOccurances); i++ {
		// search for closing brackets after the occurance, which means we are inside a tag and do not want to create nested tags
		closingBrackets := strings.Index(s[tagOccurances[i]:], "]]")
		if *debugDebug {
			log.Println("replaceFirstTag: s[:tagOccurances[i]]=", s[:tagOccurances[i]])
			log.Println("replaceFirstTag: tagReplace=", tagReplace)
			log.Println("replaceFirstTag: s[tagOccurances[i]+len(tag):]=", s[tagOccurances[i]+len(tag):])
			log.Println("replaceFirstTag")
		}
		rs := s[:tagOccurances[i]] + tagReplace + s[tagOccurances[i]+len(tag):]
		if closingBrackets < 0 {
			// no nested tag found, just replace the occurance and return
			if isTagTheWholeWord(s, tag, tagOccurances[i]) {
				return rs
			} else {
				if *debugDebug {
					log.Println("replaceFirstTag: isTagTheWholeWord was false")
				}
			}
		} else {
			if *debugDebug {
				log.Println("replaceFirstTag: s=", s)
				log.Println("replaceFirstTag: tagOccurances[i]+len(tag)=", tagOccurances[i]+len(tag))
				log.Println("replaceFirstTag: tagOccurances[i]+closingBrackets=", tagOccurances[i]+closingBrackets)
			}
			alsoOpeningBrackets := strings.Index(s[tagOccurances[i]+len(tag):tagOccurances[i]+closingBrackets], "[[")
			// search for opening brackets between end of tag found and the closing brackets found

			if alsoOpeningBrackets >= 0 {
				// no nested tag found, but a different full tag, so replace and return
				if isTagTheWholeWord(s, tag, tagOccurances[i]) {
					return rs
				} else {
					if *debugDebug {
						log.Println("replaceFirstTag: isTagTheWholeWord was false")
					}
				}
			} // else nested tag found, do nothing, continue with the next match of that tag
		}
	}
	// we only found nested tagging possibilities, so we return the original string
	return s
}

func getAllTagOccurances(s string, tag string) []int {
	if *debugDebug {
		log.Println("getAllTagOccurances: tag", tag, "in", s)
	}
	isTagAllUppercase := false
	if strings.ToUpper(tag) == tag {
		isTagAllUppercase = true
	}
	var o []int
	var r *regexp.Regexp
	var matches [][]int

	if isTagAllUppercase {
		r = regexp.MustCompile(regexp.QuoteMeta(tag))
		matches = r.FindAllStringSubmatchIndex(s, -1)
	} else {
		r = regexp.MustCompile(strings.ToLower(regexp.QuoteMeta(tag)))
		matches = r.FindAllStringSubmatchIndex(strings.ToLower(s), -1)
	}
	for _, v := range matches {
		if isTagAllUppercase {
			o = append(o, v[0])
		} else {
			// sort out all occurances being all upper case, because our tag is not all upper case
			// s[v[0]:v[0]+len(tag)] corresponds to the match string, the tag in its original case
			if strings.ToUpper(tag) != s[v[0]:v[0]+len(tag)] {
				o = append(o, v[0])
			}
		}

	}
	return o
}

// checks if a tag is the whole word or there is any letter or slash before or after the tag or dot before the tag
func isTagTheWholeWord(s string, tag string, occurance int) bool {
	if len(s) == 0 || len(tag) == 0 || occurance < 0 || isWordPartOfURL(s, tag, occurance) {
		return false
	}
	sArr := []rune(s)
	if *debugDebug {
		log.Println()
		log.Println("isTagTheWholeWord: text:", s)
		log.Println("isTagTheWholeWord: tag:", tag)
		log.Println("isTagTheWholeWord: tag as runes:", []rune(tag))
		log.Println("isTagTheWholeWord: occurance:", occurance)
		log.Println("isTagTheWholeWord: really occures at:", strings.Index(s, tag))
		log.Println("isTagTheWholeWord: sArr:", sArr)
	}
	// check the character before
	charBefore := getLastRune(s[:occurance])
	// occurance needs to be >0 in order to get the char before, which is not possible if occurance==0
	if occurance > 0 && (unicode.IsLetter(charBefore)) {
		if *debugDebug {
			log.Printf("isTagTheWholeWord: letterBefore: '%v'\n", string(getLastRune(s[:occurance])))
		}
		if *debugDebug {
			for i := 0; i < occurance-1; i++ {
				log.Printf("%v", string(sArr[i]))
			}
		}
		return false
	}
	// check the character after
	// character after the tag, is occurance plus the length of the tag plus one
	if occurance >= 0 && occurance+len(tag) < len(s) {
		charAfter := getLastRune(s[:occurance+len(tag)+1])
		if unicode.IsLetter(charAfter) {
			if *debugDebug {
				log.Printf("isTagTheWholeWord: letterAfter: '%v'\n", string(charAfter))
				for i := 0; i < occurance+len(tag); i++ {
					log.Printf("%v", string(sArr[i]))
				}
			}
			return false
		}
	}
	return true
}

func isWordPartOfURL(s string, tag string, occurance int) bool {
	if len(s) < occurance+len(tag) {
		return false
	}
	stringsBefore := strings.Fields(s[:occurance+len(tag)])
	stringsAfter := strings.Fields(s[occurance+len(tag):])

	var probablyTheURL string
	if len(stringsBefore) > 0 {
		probablyTheURL += stringsBefore[len(stringsBefore)-1]
	}
	if len(stringsAfter) > 0 {
		probablyTheURL += stringsAfter[0]
	}
	if *debugDebug {
		log.Println("isWordPartOfURL: tag", tag)
		log.Println("isWordPartOfURL: stringsBefore", stringsBefore)
		log.Println("isWordPartOfURL: stringsAfter", stringsAfter)
		log.Println("isWordPartOfURL: probablyTheURL:", probablyTheURL)
		log.Println("isWordPartOfURL: ---------------")
	}
	if strings.Contains(probablyTheURL, "://") {
		return true
	}
	return false
}

// gets the last rune in a string
func getLastRune(s string) rune {
	r := []rune(s)
	runesInString := utf8.RuneCountInString(s)
	if runesInString <= 0 {
		var nilRune rune
		return nilRune
	}
	if *debugDebug {
		log.Println("getLastRune: r:", r)
		log.Println("getLastRune: utf8.RuneCountInString(s):", runesInString)
	}
	return r[runesInString-1]
}

func readAutoTagsFromFile(autoTagsFile string) ([]string, error) {
	var a []string
	f, err := os.Open(autoTagsFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		if s.Text() != "" {
			a = append(a, s.Text())
		}
	}
	if *debug {
		log.Println("readAutoTagsFromFile: read", len(a), "auto tags from file")
	}
	return a, nil
}

func writeAutoTagsToFile(autoTagsFile string, autoTags *[]string) error {
	f, err := os.Create(autoTagsFile)
	if err != nil {
		return err
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	for _, tag := range *autoTags {
		fmt.Fprintln(w, tag)
	}
	return w.Flush()
}
