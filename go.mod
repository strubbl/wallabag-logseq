module codeberg.org/strubbl/wallabag-logseq

go 1.23.0

toolchain go1.24.0

require github.com/Strubbl/wallabago/v9 v9.0.10

require golang.org/x/text v0.23.0 // indirect

// replace github.com/Strubbl/wallabago/v9 => ../wallabago
