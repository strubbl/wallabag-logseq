package main

import (
	"errors"
	"log"
	"os"
	"time"

	"github.com/Strubbl/wallabago/v9"
)

func printElapsedTime(start time.Time) {
	if *debug {
		log.Printf("printElapsedTime: time elapsed %.2fs\n", time.Since(start).Seconds())
	}
}

// get index of articles array, where the newArticle can be found. if not found returns -1
func getKnownArticle(articles []wallabago.Item, newArticle wallabago.Item) int {
	for i := 0; i < len(articles); i++ {
		if articles[i].ID == newArticle.ID {
			return i
		}
	}
	return -1
}

func main() {
	start := time.Now()
	defer printElapsedTime(start)
	curDir, err := os.Getwd()
	if err != nil {
		log.Fatalln("error while getting current directory:", err)
	}

	if *verbose {
		log.Printf("wallabag-logseq version %s, commit %s, built at %s by %s", version, commit, date, builtBy)
	}

	log.SetOutput(os.Stdout)
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	handleFlags()

	// check for config before writing lock file to use os.Exit in case no config found
	if *verbose {
		log.Println("reading config at path", *configJSON)
	}
	exitCauseNecessaryFileDidNotExist := false
	var c WLConfig
	err = readCurrentJSON(&c, *configJSON)
	if err != nil {
		var fdneErr *FileDoesNotExistError
		if errors.As(err, &fdneErr) {
			log.Println("config file at", *configJSON, "does not exist, creating default config now")
			c = getDefaultConfig()
			cErr := writeNewJSON(c, *configJSON)
			if cErr != nil {
				log.Println("while trying to write default config to path", *configJSON, "got the error:", err)
			}
		} else {
			log.Printf("got error while reading config file at %s: %v\n", *configJSON, err)
		}
		exitCauseNecessaryFileDidNotExist = true
	}

	err = validateConfig(c)
	if err != nil {
		log.Println("validation of the given config file at", curDir+string(os.PathSeparator)+*configJSON, "failed:", err)
		exitCauseNecessaryFileDidNotExist = true
	}
	wallabago.LibConfig = c.WbgoConfig

	var autoTags []string
	if c.AutoTagsFilePath != "" {
		autoTags, err = readAutoTagsFromFile(c.AutoTagsFilePath)
		if err != nil {
			if os.IsNotExist(err) {
				log.Println("auto tags file at", c.AutoTagsFilePath, "not found, creating it now")
				wErr := writeFile(defaultAutoTag1+"\n"+defaultAutoTag2+"\n", c.AutoTagsFilePath)
				if wErr != nil {
					log.Println("error while writing auto tags file:", wErr)
				}
			} else {
				log.Println("auto tags file cannot be read:", err)
			}
			exitCauseNecessaryFileDidNotExist = true
		} else if len(autoTags) == 0 {
			log.Println("just for your information, your auto tags file has no tags defined")
		}
	} else {
		// This check is implemented, because old config files did not have that variable, but only a variable called AutoTags
		log.Fatalln("your configuration has no AutoTagsFilePath defined")
	}

	pageTemplate, err := readFile(c.PageTemplateFilePath)
	if err != nil {
		var fdneErr *FileDoesNotExistError
		if errors.As(err, &fdneErr) {
			log.Println("page template at", c.PageTemplateFilePath, "not found, creating it now")
			wErr := writeFile(defaultPageTemplate, c.PageTemplateFilePath)
			if wErr != nil {
				log.Println("error while writing page template:", wErr)
			}
		} else {
			log.Println("page template cannot be read:", err)
		}
		exitCauseNecessaryFileDidNotExist = true
	} else if pageTemplate == "" {
		if *verbose {
			log.Println("just for your information, the page template is empty")
		}
	}

	articleTemplate, err := readFile(c.ArticleTemplateFilePath)
	if err != nil {
		var fdneErr *FileDoesNotExistError
		if errors.As(err, &fdneErr) {
			log.Println("article template at", c.ArticleTemplateFilePath, "not found, creating it now")
			wErr := writeFile(defaultArticleTemplate, c.ArticleTemplateFilePath)
			if wErr != nil {
				log.Println("error while writing article template:", wErr)
			}
		} else {
			log.Println("article template cannot be read:", err)
		}
		exitCauseNecessaryFileDidNotExist = true
	} else if articleTemplate == "" {
		log.Println("article template is empty which makes no sense, please fix this")
		exitCauseNecessaryFileDidNotExist = true
	}

	if exitCauseNecessaryFileDidNotExist {
		os.Exit(1)
	}

	// check config
	if c.WbgoConfig.WallabagURL == defaultWbgoConfigWallabagURL || c.WbgoConfig.ClientID == defaultWbgoConfigClientID || c.WbgoConfig.ClientSecret == defaultWbgoConfigClientSecret {
		log.Fatalln("config still has default values, exiting")
	}

	// learn tags for auto-tagging
	if *learnTags {
		if *debug {
			log.Println("learning the tags for auto-tagging")
		}
		err := learnTagsForAutoTagging(c, &autoTags)
		if err != nil {
			log.Fatalln("error while learning the tags for auto-tagging")
		}
		err = writeAutoTagsToFile(c.AutoTagsFilePath, &autoTags)
		if err != nil {
			log.Fatalln("error while writing the learned auto tags to the file")
		}
		os.Exit(0)
	}

	// check for cached data
	if *verbose {
		log.Println("reading cache", c.CachingFilePath)
	}
	var cache Caching
	err = readCurrentJSON(&cache, c.CachingFilePath)
	if err != nil {
		log.Printf("got error while reading cache file: %v\n", err)
	}

	if *verbose {
		log.Println("get articles with annotations from wallabag")
	}
	articlesWithAnno, err := wallabago.GetAllEntriesWithAnnotations(int(cache.SinceTime), "archived", "asc")
	if err != nil {
		log.Printf("got error while getting articles with annotations: %v\n", err)
	}
	// remove content from articles. not needed for this use case
	for i := 0; i < len(articlesWithAnno); i++ {
		articlesWithAnno[i].Content = ""
	}

	// recreate markdown from cache if markdown path does not exist
	if _, err := os.Stat(c.LogseqMarkdownFilePath); errors.Is(err, os.ErrNotExist) {
		for i := 0; i < len(cache.AnnotatedArticles); i++ {
			err = appendArticleToMdFile(cache.AnnotatedArticles[i], c.LogseqMarkdownFilePath, pageTemplate, articleTemplate, autoTags)
			if err != nil {
				log.Println("got error while appending the", i+1, ". article to markdown file:", err)
			}
		}
	}
	// when cache is empty, use all the new articles
	if len(cache.AnnotatedArticles) == 0 {
		cache.AnnotatedArticles = articlesWithAnno
		for i := 0; i < len(cache.AnnotatedArticles); i++ {
			err = appendArticleToMdFile(cache.AnnotatedArticles[i], c.LogseqMarkdownFilePath, pageTemplate, articleTemplate, autoTags)
			if err != nil {
				log.Printf("got error while appending to markdown file: %v\n", err)
			}
		}
	} else {
		// cache is not empty, so loop through all new articles
		for i := 0; i < len(articlesWithAnno); i++ {
			whichKnown := getKnownArticle(cache.AnnotatedArticles, articlesWithAnno[i])
			// when article is known in cache but UpdatedAt is different
			if whichKnown >= 0 && cache.AnnotatedArticles[whichKnown].UpdatedAt != articlesWithAnno[i].UpdatedAt {
				if len(cache.AnnotatedArticles[whichKnown].Annotations) == len(articlesWithAnno[i].Annotations) {
					// number of annotations is still the same, so comparing annotation text and quote for any difference
					for k := 0; k < len(cache.AnnotatedArticles[whichKnown].Annotations); k++ {
						if cache.AnnotatedArticles[whichKnown].Annotations[k].Quote != articlesWithAnno[i].Annotations[k].Quote || cache.AnnotatedArticles[whichKnown].Annotations[k].Text != articlesWithAnno[i].Annotations[k].Text {
							// annotation quote or text is different
							log.Println("The following article is already known, but got an updated annotation meanwhile. This means, the article is appended again to the markdown file. Please take care of merging the duplicate article:", articlesWithAnno[i])
							cache.AnnotatedArticles[whichKnown] = articlesWithAnno[i]
							err = appendArticleToMdFile(articlesWithAnno[i], c.LogseqMarkdownFilePath, pageTemplate, articleTemplate, autoTags)
							if err != nil {
								log.Printf("got error while appending to markdown file: %v\n", err)
							}
						}
					}
				} else {
					// number of annotation is different
					log.Println("The following article is already known but has new annotations since the last time we checked. This means, the article is appended again to the markdown file. Please take care of merging the duplicate article:", articlesWithAnno[i])
					cache.AnnotatedArticles[whichKnown] = articlesWithAnno[i]
					err = appendArticleToMdFile(articlesWithAnno[i], c.LogseqMarkdownFilePath, pageTemplate, articleTemplate, autoTags)
					if err != nil {
						log.Printf("got error while appending to markdown file: %v\n", err)
					}
				}
			} else if whichKnown < 0 {
				// article is unknown yet, appending it to cache and markdown
				cache.AnnotatedArticles = append(cache.AnnotatedArticles, articlesWithAnno[i])
				err = appendArticleToMdFile(articlesWithAnno[i], c.LogseqMarkdownFilePath, pageTemplate, articleTemplate, autoTags)
				if err != nil {
					log.Printf("got error while appending to markdown file: %v\n", err)
				}
			} else {
				// article is known and UpdatedAt is the same, how can this happen?
				log.Println("old:", cache.AnnotatedArticles[whichKnown])
				log.Println("new:", articlesWithAnno[i])
				log.Fatalln("Article is already known, but the UpdatedAt field is equal in new and old article. This cannot happen when using the since query.")
			}
		}
	}
	cache.SinceTime = start.Unix()
	err = writeNewJSON(cache, c.CachingFilePath)
	if err != nil {
		log.Printf("got error while writing cache file: %v\n", err)
	}
}
