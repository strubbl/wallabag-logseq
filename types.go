package main

import (
	"github.com/Strubbl/wallabago/v9"
)

type Caching struct {
	SinceTime         int64
	AnnotatedArticles []wallabago.Item
}

type WLConfig struct {
	ArticleTemplateFilePath string
	AutoTagsFilePath        string
	CachingFilePath         string
	LogseqMarkdownFilePath  string
	PageTemplateFilePath    string
	WbgoConfig              wallabago.WallabagConfig
}
