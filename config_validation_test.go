package main

import (
	"errors"
	"testing"

	"github.com/Strubbl/wallabago/v9"
)

func TestCalidateConfig(t *testing.T) {
	var tests = []struct {
		c WLConfig
		e error
	}{
		{WLConfig{}, InvalidConfigError{Msg: "WbgoConfig.ClientID is empty. WbgoConfig.ClientSecret is empty. WbgoConfig.UserName is empty. WbgoConfig.UserPassword is empty. WbgoConfig.WallabagURL is empty. "}},
		{WLConfig{WbgoConfig: wallabago.WallabagConfig{ClientID: "", ClientSecret: "", UserName: "", UserPassword: "", WallabagURL: ""}}, InvalidConfigError{Msg: "WbgoConfig.ClientID is empty. WbgoConfig.ClientSecret is empty. WbgoConfig.UserName is empty. WbgoConfig.UserPassword is empty. WbgoConfig.WallabagURL is empty. "}},
		{WLConfig{WbgoConfig: wallabago.WallabagConfig{ClientID: "a", ClientSecret: "a", UserName: "a", UserPassword: "a", WallabagURL: "a"}}, InvalidConfigError{Msg: "WbgoConfig.WallabagURL does not start with http:// nor https://. "}},
		{WLConfig{WbgoConfig: wallabago.WallabagConfig{ClientID: "a", ClientSecret: "a", UserName: "a", UserPassword: "a", WallabagURL: "http://a"}}, nil},
		{WLConfig{WbgoConfig: wallabago.WallabagConfig{ClientID: "a", ClientSecret: "a", UserName: "a", UserPassword: "a", WallabagURL: "https://a"}}, nil},
	}
	for _, test := range tests {
		r := validateConfig(test.c)
		if !errors.Is(r, test.e) {
			t.Errorf("\nwanted : %v\ngot    : %v\n", test.e, r)
		}
	}
}
