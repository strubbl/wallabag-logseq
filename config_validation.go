package main

import (
	"fmt"
	"strings"
)

type InvalidConfigError struct {
	Msg string
}

func (e InvalidConfigError) Error() string {
	return fmt.Sprintf("invalid config: %s", e.Msg)
}

func (err InvalidConfigError) Is(target error) bool {
	if target, ok := target.(InvalidConfigError); ok {
		return target.Msg == "" || target.Msg == err.Msg
	}
	return false
}

// do some validations on the config read from config.json
func validateConfig(wbgLogseqConfig WLConfig) error {
	// TODO add more validations besides the WbgoConfig
	errmsg := ""
	if wbgLogseqConfig.WbgoConfig.ClientID == "" {
		errmsg += "WbgoConfig.ClientID is empty. "
	}
	if wbgLogseqConfig.WbgoConfig.ClientSecret == "" {
		errmsg += "WbgoConfig.ClientSecret is empty. "
	}
	if wbgLogseqConfig.WbgoConfig.UserName == "" {
		errmsg += "WbgoConfig.UserName is empty. "
	}
	if wbgLogseqConfig.WbgoConfig.UserPassword == "" {
		errmsg += "WbgoConfig.UserPassword is empty. "
	}
	// WallabagURL checks
	if wbgLogseqConfig.WbgoConfig.WallabagURL == "" {
		errmsg += "WbgoConfig.WallabagURL is empty. "
	} else if !strings.HasPrefix(wbgLogseqConfig.WbgoConfig.WallabagURL, "http://") && !strings.HasPrefix(wbgLogseqConfig.WbgoConfig.WallabagURL, "https://") {
		errmsg += "WbgoConfig.WallabagURL does not start with http:// nor https://. "
	}
	if errmsg == "" {
		return nil
	} else {
		return &InvalidConfigError{Msg: errmsg}
	}
}
