package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
)

type FileDoesNotExistError struct{}

func (e *FileDoesNotExistError) Error() string {
	return "file does not exist"
}

func getDefaultConfig() WLConfig {
	var c WLConfig
	if *debugDebug {
		log.Println("empty config:", c)
	}
	c.ArticleTemplateFilePath = defaultTemplateFilePathArticle
	c.AutoTagsFilePath = defaultAutoTagsFilePath
	c.CachingFilePath = defaultCachingJSON
	c.LogseqMarkdownFilePath = defaultOutputPath
	c.PageTemplateFilePath = defaultTemplateFilePathPage
	c.WbgoConfig.ClientID = defaultWbgoConfigClientID
	c.WbgoConfig.ClientSecret = defaultWbgoConfigClientSecret
	c.WbgoConfig.WallabagURL = defaultWbgoConfigWallabagURL
	c.WbgoConfig.UserName = defaultWbgoConfigUserName
	c.WbgoConfig.UserPassword = defaultWbgoConfigUserPassword
	if *debugDebug {
		log.Println("prefilled config:", c)
	}
	return c
}

func readCurrentJSON(i interface{}, fp string) error {
	if *debugDebug {
		log.Println("readCurrentJSON")
	}
	if *debugDebug {
		log.Println("readCurrentJSON: file path is", fp, "with given type:")
		log.Printf("%T\n", i)
	}
	switch i.(type) {
	case *Caching:
		if *debug {
			log.Println("found *Caching type")
		}
	case *WLConfig:
		if *debug {
			log.Println("found *WLConfig type")
		}
	default:
		log.Fatalln("unkown type for reading json")
	}

	if _, err := os.Stat(fp); os.IsNotExist(err) {
		if *verbose { // not fatal, just start with a new one
			log.Printf("file does not exist %s", fp)
		}
		return &FileDoesNotExistError{}
	}
	b, err := os.ReadFile(fp)
	if err != nil {
		if *debug {
			log.Println("readCurrentJSON: error while os.ReadFile", err)
		}
		fmt.Println(err)
		return err
	}
	err = json.Unmarshal(b, i)
	if err != nil {
		if *debug {
			log.Println("readCurrentJSON: error while json.Unmarshal", err)
		}
		return err
	}
	return nil
}

func writeNewJSON(i interface{}, fp string) error {
	if *debugDebug {
		log.Println("writeNewJSON: given type:")
		log.Printf("%T\n", i)
	}
	switch i.(type) {
	case Caching:
		if *debug {
			log.Println("found Caching type")
		}
	case WLConfig:
		if *debug {
			log.Println("found WLConfig type")
		}
	default:
		return errors.New("unkown data type for writing json")
	}

	b, err := json.MarshalIndent(i, "", "  ")
	if err != nil {
		if *debug {
			log.Println("writeNewJSON: error while marshalling data json", err)
		}
		return err
	}
	err = os.WriteFile(fp, b, 0644)
	if err != nil {
		if *debug {
			log.Println("writeNewJSON: error while writing data json", err)
		}
		return err
	}
	return nil
}
